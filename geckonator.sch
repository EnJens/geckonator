EESchema Schematic File Version 4
LIBS:geckonator-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L EFM32HG:EFM32HG310F64G-B-QFN32 U1
U 1 1 5A7F8B23
P 3800 3800
F 0 "U1" H 3200 4800 50  0000 C CNN
F 1 "EFM32HG310F64G-B-QFN32" H 3800 3800 50  0000 C CNN
F 2 "Package_DFN_QFN_Extra:QFN-32_EP_6x6_Pitch0.65mm" H 3750 3800 50  0001 C CNN
F 3 "" H 3750 3800 50  0001 C CNN
	1    3800 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR01
U 1 1 5A7F8E9E
P 3450 2400
F 0 "#PWR01" H 3450 2250 50  0001 C CNN
F 1 "+3V3" H 3465 2573 50  0000 C CNN
F 2 "" H 3450 2400 50  0001 C CNN
F 3 "" H 3450 2400 50  0001 C CNN
	1    3450 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2650 3450 2475
Wire Wire Line
	3550 2650 3550 2475
Wire Wire Line
	3550 2475 3450 2475
Connection ~ 3450 2475
Wire Wire Line
	3450 2475 3450 2400
Wire Wire Line
	3650 2650 3650 2475
Wire Wire Line
	3650 2475 3550 2475
Connection ~ 3550 2475
Wire Wire Line
	3750 2650 3750 2475
Wire Wire Line
	3750 2475 3650 2475
Connection ~ 3650 2475
Wire Wire Line
	3850 2650 3850 2475
Wire Wire Line
	3850 2475 3750 2475
Connection ~ 3750 2475
Wire Wire Line
	3950 2650 3950 2475
Wire Wire Line
	3950 2475 3850 2475
Connection ~ 3850 2475
$Comp
L power:VBUS #PWR02
U 1 1 5A7F905F
P 4050 2375
F 0 "#PWR02" H 4050 2225 50  0001 C CNN
F 1 "VBUS" H 4065 2548 50  0000 C CNN
F 2 "" H 4050 2375 50  0001 C CNN
F 3 "" H 4050 2375 50  0001 C CNN
	1    4050 2375
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2650 4050 2375
$Comp
L power:+3V3 #PWR03
U 1 1 5A7F90F4
P 4150 2475
F 0 "#PWR03" H 4150 2325 50  0001 C CNN
F 1 "+3V3" H 4165 2648 50  0000 C CNN
F 2 "" H 4150 2475 50  0001 C CNN
F 3 "" H 4150 2475 50  0001 C CNN
	1    4150 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2650 4150 2550
$Comp
L power:GND #PWR07
U 1 1 5A7F930C
P 1800 1225
F 0 "#PWR07" H 1800 975 50  0001 C CNN
F 1 "GND" H 1805 1052 50  0000 C CNN
F 2 "" H 1800 1225 50  0001 C CNN
F 3 "" H 1800 1225 50  0001 C CNN
	1    1800 1225
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR04
U 1 1 5A7F93A8
P 1050 800
F 0 "#PWR04" H 1050 650 50  0001 C CNN
F 1 "+3V3" H 1065 973 50  0000 C CNN
F 2 "" H 1050 800 50  0001 C CNN
F 3 "" H 1050 800 50  0001 C CNN
	1    1050 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5A7F93FE
P 1200 1000
F 0 "C1" H 1175 1075 50  0000 R CNN
F 1 "10uF" H 1300 900 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1200 1000 50  0001 C CNN
F 3 "~" H 1200 1000 50  0001 C CNN
	1    1200 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5A7F9424
P 1375 1000
F 0 "C2" H 1250 1075 50  0000 L CNN
F 1 "100nF" H 1275 950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1375 1000 50  0001 C CNN
F 3 "~" H 1375 1000 50  0001 C CNN
	1    1375 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5A7F9454
P 1550 1000
F 0 "C3" H 1450 1075 50  0000 L CNN
F 1 "100nF" H 1425 875 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1550 1000 50  0001 C CNN
F 3 "~" H 1550 1000 50  0001 C CNN
	1    1550 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 800  1050 850 
Wire Wire Line
	1050 850  1200 850 
Wire Wire Line
	1550 900  1550 850 
Wire Wire Line
	1375 850  1375 900 
Connection ~ 1375 850 
Wire Wire Line
	1375 850  1550 850 
Wire Wire Line
	1200 900  1200 850 
Connection ~ 1200 850 
Wire Wire Line
	1200 850  1375 850 
Wire Wire Line
	1200 1150 1200 1100
Wire Wire Line
	1375 1100 1375 1150
Connection ~ 1375 1150
Wire Wire Line
	1375 1150 1200 1150
Wire Wire Line
	1550 1100 1550 1150
Wire Wire Line
	1550 1150 1375 1150
Wire Wire Line
	1800 1225 1800 1150
Text Notes 1675 625  0    50   ~ 0
IOVDD
Wire Notes Line
	675  525  1925 525 
Wire Notes Line
	1925 525  1925 1500
Wire Notes Line
	1925 1500 675  1500
Wire Notes Line
	675  1500 675  525 
Text Notes 4150 625  0    50   ~ 0
USBREG
Wire Notes Line
	3225 525  4475 525 
Wire Notes Line
	4475 525  4475 1500
Wire Notes Line
	4475 1500 3225 1500
Wire Notes Line
	3225 1500 3225 525 
$Comp
L power:VBUS #PWR010
U 1 1 5A802813
P 3425 800
F 0 "#PWR010" H 3425 650 50  0001 C CNN
F 1 "VBUS" H 3440 973 50  0000 C CNN
F 2 "" H 3425 800 50  0001 C CNN
F 3 "" H 3425 800 50  0001 C CNN
	1    3425 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5A8028C7
P 3425 1000
F 0 "C9" H 3517 1046 50  0000 L CNN
F 1 "4.7uF" H 3517 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3425 1000 50  0001 C CNN
F 3 "~" H 3425 1000 50  0001 C CNN
	1    3425 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5A802A25
P 3425 1225
F 0 "#PWR011" H 3425 975 50  0001 C CNN
F 1 "GND" H 3430 1052 50  0000 C CNN
F 2 "" H 3425 1225 50  0001 C CNN
F 3 "" H 3425 1225 50  0001 C CNN
	1    3425 1225
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 800  3425 900 
Wire Wire Line
	3425 1100 3425 1225
$Comp
L power:VBUS #PWR06
U 1 1 5A8077CE
P 1750 5750
F 0 "#PWR06" H 1750 5600 50  0001 C CNN
F 1 "VBUS" H 1765 5923 50  0000 C CNN
F 2 "" H 1750 5750 50  0001 C CNN
F 3 "" H 1750 5750 50  0001 C CNN
	1    1750 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1625 5925 1750 5925
Wire Wire Line
	1750 5925 1750 5750
Wire Wire Line
	1625 6125 1925 6125
Wire Wire Line
	1625 6225 1925 6225
$Comp
L power:GND #PWR05
U 1 1 5A80A2D2
P 1325 6675
F 0 "#PWR05" H 1325 6425 50  0001 C CNN
F 1 "GND" H 1330 6502 50  0000 C CNN
F 2 "" H 1325 6675 50  0001 C CNN
F 3 "" H 1325 6675 50  0001 C CNN
	1    1325 6675
	1    0    0    -1  
$EndComp
Wire Wire Line
	1325 6600 1325 6675
Wire Wire Line
	1325 6525 1325 6600
Connection ~ 1325 6600
Text Label 1675 6125 0    50   ~ 0
USB_D+
Text Label 1675 6225 0    50   ~ 0
USB_D-
$Comp
L Device:C_Small C11
U 1 1 5A80C7C6
P 3850 5125
F 0 "C11" H 3942 5171 50  0000 L CNN
F 1 "1uF" H 3942 5080 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3850 5125 50  0001 C CNN
F 3 "~" H 3850 5125 50  0001 C CNN
	1    3850 5125
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5A80C86D
P 3850 5325
F 0 "#PWR014" H 3850 5075 50  0001 C CNN
F 1 "GND" H 3855 5152 50  0000 C CNN
F 2 "" H 3850 5325 50  0001 C CNN
F 3 "" H 3850 5325 50  0001 C CNN
	1    3850 5325
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4950 3750 5275
Wire Wire Line
	3750 5275 3850 5275
Wire Wire Line
	3850 5275 3850 5325
Wire Wire Line
	3850 5225 3850 5275
Connection ~ 3850 5275
Wire Wire Line
	3850 4950 3850 4975
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5A81084F
P 1950 5925
F 0 "#FLG01" H 1950 6000 50  0001 C CNN
F 1 "PWR_FLAG" V 1950 6053 50  0000 L CNN
F 2 "" H 1950 5925 50  0001 C CNN
F 3 "" H 1950 5925 50  0001 C CNN
	1    1950 5925
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 3950 5075 3950
Wire Wire Line
	4650 4050 5075 4050
Text Label 4725 3950 0    50   ~ 0
USB_D+
Text Label 4725 4050 0    50   ~ 0
USB_D-
$Comp
L power:GND #PWR015
U 1 1 5A819D9C
P 5300 1225
F 0 "#PWR015" H 5300 975 50  0001 C CNN
F 1 "GND" H 5305 1052 50  0000 C CNN
F 2 "" H 5300 1225 50  0001 C CNN
F 3 "" H 5300 1225 50  0001 C CNN
	1    5300 1225
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR016
U 1 1 5A819DA2
P 4875 800
F 0 "#PWR016" H 4875 650 50  0001 C CNN
F 1 "+3V3" H 4890 973 50  0000 C CNN
F 2 "" H 4875 800 50  0001 C CNN
F 3 "" H 4875 800 50  0001 C CNN
	1    4875 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C13
U 1 1 5A819DAE
P 5200 1000
F 0 "C13" H 5075 1075 50  0000 L CNN
F 1 "100nF" H 5100 950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5200 1000 50  0001 C CNN
F 3 "~" H 5200 1000 50  0001 C CNN
	1    5200 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4875 800  4875 850 
Wire Wire Line
	5200 850  5200 900 
Wire Wire Line
	5200 1100 5200 1150
Text Notes 5350 625  0    50   ~ 0
VDD_DREG
Wire Notes Line
	4500 525  5750 525 
Wire Notes Line
	5750 1500 4500 1500
Wire Notes Line
	4500 1500 4500 525 
Wire Notes Line
	5750 1500 5750 525 
Wire Wire Line
	1950 5925 1750 5925
Connection ~ 1750 5925
Wire Wire Line
	5200 1150 5300 1150
Wire Wire Line
	5300 1150 5300 1225
$Comp
L Switch:SW_Push SW1
U 1 1 5A825942
P 2500 3950
F 0 "SW1" H 2500 4250 50  0000 C CNN
F 1 "RST_BTN" H 2500 4150 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_FSMSM" H 2500 4150 50  0001 C CNN
F 3 "" H 2500 4150 50  0001 C CNN
	1    2500 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 3950 2700 3950
$Comp
L Device:LED D2
U 1 1 5A836FD7
P 4925 2550
F 0 "D2" H 4917 2295 50  0000 C CNN
F 1 "PWR_LED" H 4917 2386 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 4925 2550 50  0001 C CNN
F 3 "~" H 4925 2550 50  0001 C CNN
	1    4925 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5A83744C
P 4575 2550
F 0 "R2" V 4379 2550 50  0000 C CNN
F 1 "360R" V 4470 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4575 2550 50  0001 C CNN
F 3 "~" H 4575 2550 50  0001 C CNN
	1    4575 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	4475 2550 4150 2550
Connection ~ 4150 2550
Wire Wire Line
	4150 2550 4150 2475
Wire Wire Line
	4775 2550 4675 2550
$Comp
L power:GND #PWR021
U 1 1 5A83D149
P 5150 2625
F 0 "#PWR021" H 5150 2375 50  0001 C CNN
F 1 "GND" H 5155 2452 50  0000 C CNN
F 2 "" H 5150 2625 50  0001 C CNN
F 3 "" H 5150 2625 50  0001 C CNN
	1    5150 2625
	1    0    0    -1  
$EndComp
Wire Wire Line
	5075 2550 5150 2550
Wire Wire Line
	5150 2550 5150 2625
Wire Wire Line
	4650 3650 5075 3650
Wire Wire Line
	4650 3550 5075 3550
Text Label 4700 3550 0    50   ~ 0
SWDIO
Text Label 4700 3650 0    50   ~ 0
SWCLK
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5A8B3682
P 1425 6600
F 0 "#FLG02" H 1425 6675 50  0001 C CNN
F 1 "PWR_FLAG" V 1425 6728 50  0000 L CNN
F 2 "" H 1425 6600 50  0001 C CNN
F 3 "" H 1425 6600 50  0001 C CNN
	1    1425 6600
	0    1    1    0   
$EndComp
Wire Wire Line
	1425 6600 1325 6600
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5A8B77B0
P 4025 4975
F 0 "#FLG03" H 4025 5050 50  0001 C CNN
F 1 "PWR_FLAG" V 4025 5103 50  0000 L CNN
F 2 "" H 4025 4975 50  0001 C CNN
F 3 "" H 4025 4975 50  0001 C CNN
	1    4025 4975
	0    1    1    0   
$EndComp
Wire Wire Line
	4025 4975 3850 4975
Connection ~ 3850 4975
Wire Wire Line
	3850 4975 3850 5025
$Comp
L Connector_Generic:Conn_01x04_Male J4
U 1 1 5A8BB880
P 8175 4925
F 0 "J4" H 8147 4898 50  0000 R CNN
F 1 "Conn_01x04_Male" H 8147 4807 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 8175 4925 50  0001 C CNN
F 3 "~" H 8175 4925 50  0001 C CNN
	1    8175 4925
	1    0    0    -1  
$EndComp
Text Label 3850 4975 0    50   ~ 0
DECOUPLE
Wire Wire Line
	8375 4825 8925 4825
Wire Wire Line
	8375 4925 8925 4925
Wire Wire Line
	8375 5025 8925 5025
Wire Wire Line
	8375 5125 8925 5125
Text Label 8425 4825 0    50   ~ 0
SWDIO
Text Label 8425 5025 0    50   ~ 0
SWCLK
$Comp
L power:GND #PWR025
U 1 1 5A9D63AC
P 8925 4925
F 0 "#PWR025" H 8925 4675 50  0001 C CNN
F 1 "GND" V 8930 4797 50  0000 R CNN
F 2 "" H 8925 4925 50  0001 C CNN
F 3 "" H 8925 4925 50  0001 C CNN
	1    8925 4925
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR026
U 1 1 5A9DB900
P 8925 5125
F 0 "#PWR026" H 8925 4975 50  0001 C CNN
F 1 "+3V3" V 8940 5253 50  0000 L CNN
F 2 "" H 8925 5125 50  0001 C CNN
F 3 "" H 8925 5125 50  0001 C CNN
	1    8925 5125
	0    1    1    0   
$EndComp
NoConn ~ 2950 300 
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J2
U 1 1 5AA1637D
P 7025 3200
F 0 "J2" H 7075 3717 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 7075 3626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x07_P2.54mm_Vertical" H 7025 3200 50  0001 C CNN
F 3 "~" H 7025 3200 50  0001 C CNN
	1    7025 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 3050 2575 3050
Wire Wire Line
	2950 3150 2575 3150
Wire Wire Line
	2950 3250 2575 3250
Wire Wire Line
	2950 3350 2575 3350
Wire Wire Line
	2950 3450 2575 3450
Wire Wire Line
	2950 3550 2575 3550
Wire Wire Line
	2950 3650 2575 3650
Wire Wire Line
	4650 3050 5075 3050
Wire Wire Line
	4650 3150 5075 3150
Wire Wire Line
	4650 3250 5075 3250
Text Label 2675 3050 0    50   ~ 0
PA0
Text Label 2675 3150 0    50   ~ 0
PA1
Text Label 2675 3250 0    50   ~ 0
PA2
Text Label 2675 3350 0    50   ~ 0
PC0
Text Label 2675 3450 0    50   ~ 0
PC1
Text Label 2675 3550 0    50   ~ 0
PB7
Text Label 2675 3650 0    50   ~ 0
PB8
Text Label 4725 3050 0    50   ~ 0
PE13
Text Label 4725 3150 0    50   ~ 0
PE12
Text Label 4725 3250 0    50   ~ 0
PE11
NoConn ~ 4650 3450
NoConn ~ 4650 3350
$Comp
L Connector_Specialized:USB_A J1
U 1 1 5AA421E3
P 1325 6125
F 0 "J1" H 1380 6592 50  0000 C CNN
F 1 "USB_A" H 1380 6501 50  0000 C CNN
F 2 "Connector_USB_Extra:usb-PCB" H 1475 6075 50  0001 C CNN
F 3 "" H 1475 6075 50  0001 C CNN
	1    1325 6125
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5AA4244A
P 2400 4050
F 0 "R1" V 2204 4050 50  0000 C CNN
F 1 "360R" V 2295 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2400 4050 50  0001 C CNN
F 3 "~" H 2400 4050 50  0001 C CNN
	1    2400 4050
	0    1    1    0   
$EndComp
$Comp
L Device:LED_Small D1
U 1 1 5AA4258B
P 2075 4050
F 0 "D1" H 2075 4285 50  0000 C CNN
F 1 "LED_Small" H 2075 4194 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 2075 4050 50  0001 C CNN
F 3 "~" V 2075 4050 50  0001 C CNN
	1    2075 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 4050 2500 4050
Wire Wire Line
	2300 4050 2175 4050
$Comp
L Device:R_Small R4
U 1 1 5AA46BB6
P 2400 4250
F 0 "R4" V 2204 4250 50  0000 C CNN
F 1 "360R" V 2295 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2400 4250 50  0001 C CNN
F 3 "~" H 2400 4250 50  0001 C CNN
	1    2400 4250
	0    1    1    0   
$EndComp
$Comp
L Device:LED_Small D4
U 1 1 5AA46BBC
P 2075 4250
F 0 "D4" H 2075 4485 50  0000 C CNN
F 1 "LED_Small" H 2075 4394 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 2075 4250 50  0001 C CNN
F 3 "~" V 2075 4250 50  0001 C CNN
	1    2075 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 4250 2500 4250
Wire Wire Line
	2300 4250 2175 4250
$Comp
L Device:R_Small R3
U 1 1 5AA4B758
P 2400 4150
F 0 "R3" V 2204 4150 50  0000 C CNN
F 1 "360R" V 2295 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2400 4150 50  0001 C CNN
F 3 "~" H 2400 4150 50  0001 C CNN
	1    2400 4150
	0    1    1    0   
$EndComp
$Comp
L Device:LED_Small D3
U 1 1 5AA4B75E
P 2075 4150
F 0 "D3" H 2075 4385 50  0000 C CNN
F 1 "LED_Small" H 2075 4294 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 2075 4150 50  0001 C CNN
F 3 "~" V 2075 4150 50  0001 C CNN
	1    2075 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 4150 2500 4150
Wire Wire Line
	2300 4150 2175 4150
$Comp
L Switch:SW_Push SW2
U 1 1 5AA4E0BB
P 2575 4350
F 0 "SW2" H 2575 4257 50  0000 C CNN
F 1 "USR_BTN" H 2575 4166 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_FSMSM" H 2575 4550 50  0001 C CNN
F 3 "" H 2575 4550 50  0001 C CNN
	1    2575 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 4350 2775 4350
$Comp
L power:GND #PWR012
U 1 1 5AA50BB8
P 1850 4425
F 0 "#PWR012" H 1850 4175 50  0001 C CNN
F 1 "GND" H 1855 4252 50  0000 C CNN
F 2 "" H 1850 4425 50  0001 C CNN
F 3 "" H 1850 4425 50  0001 C CNN
	1    1850 4425
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3950 1850 4050
Wire Wire Line
	1850 3950 2300 3950
Wire Wire Line
	2375 4350 1850 4350
Connection ~ 1850 4350
Wire Wire Line
	1850 4350 1850 4425
Wire Wire Line
	1975 4250 1850 4250
Connection ~ 1850 4250
Wire Wire Line
	1850 4250 1850 4350
Wire Wire Line
	1975 4150 1850 4150
Connection ~ 1850 4150
Wire Wire Line
	1850 4150 1850 4250
Wire Wire Line
	1975 4050 1850 4050
Connection ~ 1850 4050
Wire Wire Line
	1850 4050 1850 4150
NoConn ~ 4650 4150
NoConn ~ 4650 4250
NoConn ~ 1225 6525
$Comp
L power:VBUS #PWR013
U 1 1 5A814A5A
P 6675 2900
F 0 "#PWR013" H 6675 2750 50  0001 C CNN
F 1 "VBUS" V 6690 3027 50  0000 L CNN
F 2 "" H 6675 2900 50  0001 C CNN
F 3 "" H 6675 2900 50  0001 C CNN
	1    6675 2900
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR017
U 1 1 5A814BD9
P 6675 3000
F 0 "#PWR017" H 6675 2850 50  0001 C CNN
F 1 "+3V3" V 6690 3128 50  0000 L CNN
F 2 "" H 6675 3000 50  0001 C CNN
F 3 "" H 6675 3000 50  0001 C CNN
	1    6675 3000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5A814CD9
P 7500 2900
F 0 "#PWR018" H 7500 2650 50  0001 C CNN
F 1 "GND" V 7505 2772 50  0000 R CNN
F 2 "" H 7500 2900 50  0001 C CNN
F 3 "" H 7500 2900 50  0001 C CNN
	1    7500 2900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5A814D85
P 7500 3000
F 0 "#PWR019" H 7500 2750 50  0001 C CNN
F 1 "GND" V 7505 2872 50  0000 R CNN
F 2 "" H 7500 3000 50  0001 C CNN
F 3 "" H 7500 3000 50  0001 C CNN
	1    7500 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7500 3000 7325 3000
Wire Wire Line
	7500 2900 7325 2900
Wire Wire Line
	6825 2900 6675 2900
Wire Wire Line
	6825 3000 6675 3000
Wire Wire Line
	6825 3100 6575 3100
Wire Wire Line
	6825 3200 6575 3200
Wire Wire Line
	6825 3300 6575 3300
Wire Wire Line
	6825 3400 6575 3400
Wire Wire Line
	6825 3500 6575 3500
Wire Wire Line
	7325 3300 7550 3300
Wire Wire Line
	7325 3200 7550 3200
Wire Wire Line
	7325 3100 7550 3100
Wire Wire Line
	7325 3400 7550 3400
Wire Wire Line
	7325 3500 7550 3500
Text Label 6650 3100 0    50   ~ 0
PB8
Text Label 7350 3100 0    50   ~ 0
PB7
Text Label 6650 3200 0    50   ~ 0
PC1
Text Label 7350 3200 0    50   ~ 0
PC0
Text Label 6650 3300 0    50   ~ 0
PA2
Text Label 7350 3300 0    50   ~ 0
PA1
Text Label 6650 3400 0    50   ~ 0
PA0
Text Label 7350 3400 0    50   ~ 0
PE13
Text Label 6650 3500 0    50   ~ 0
PE12
Text Label 7350 3500 0    50   ~ 0
PE11
Text Label 2675 3950 0    50   ~ 0
RST
Text Label 2750 4350 0    50   ~ 0
USB_BTN
Wire Wire Line
	1550 1150 1800 1150
Connection ~ 1550 1150
Wire Wire Line
	4875 850  5200 850 
$EndSCHEMATC
